import pygame
import numpy as np
import sys
import random

pygame.init()

display_width = 700
display_height = 700

photoSurface = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('raster face')
photoImg = pygame.image.load('raster-face-700-bw.jpg').convert()
photoImgInv = pygame.image.load('raster-face-700-bw-2.jpg').convert()

clock = pygame.time.Clock()
crashed = False

def makeFrame():
    pixAr = pygame.surfarray.pixels2d(photoImgInv)
    # pixAr ^= 2 ** 32 - 1

    np.random.shuffle(pixAr)
    np.random.shuffle([np.random.shuffle(c.T) for c in pixAr])

    rand = random.randint(0, 699)
    randColor = random.randint(0, 16777215)
    pixAr[:, rand] = randColor

    del pixAr


time = 750
while not crashed:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True

    photoSurface.blit(photoImg, (0, 0))
    passed_time = clock.tick(30)
    time -= passed_time
    if time <= 30:
        photoSurface.blit(photoImgInv, (0, 0))
        makeFrame()

    pygame.display.update()
    clock.tick(30)


pygame.quit()
quit()
